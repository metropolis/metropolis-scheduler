import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
		host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)

channel.queue_declare(queue='result_queue', durable=True)


channel.queue_purge(queue='task_queue')
channel.queue_purge(queue='result_queue')


connection.close()