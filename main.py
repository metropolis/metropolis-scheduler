#!/usr/bin/python


import requests
from pprint import pprint
import json
import numpy as np
import threading
import time
import pika



class RMQScheduler(object):
	def __init__(self):
		self.n_Zones = 9
		self.zones = [[]  for x in xrange(self.n_Zones) ]
		self.poll = range(self.n_Zones) * 5
		self.probabilities = [0.9] * self.n_Zones
		self.n_tasks = len(self.poll)

		self.connection = pika.BlockingConnection(pika.ConnectionParameters(
        		host='localhost'))
		self.channel = self.connection.channel()

		self.channel.queue_declare(queue='task_queue', durable=True)
		
		self.channel.queue_declare(queue='result_queue', durable=True)

		
		self.channel.queue_purge(queue='task_queue')
		self.channel.queue_purge(queue='result_queue')

		self.channel.basic_qos(prefetch_count=1)
		self.channel.basic_consume(self.callback,
        	              queue='result_queue')



		self.t = threading.Thread(target=self.start_consuming)
		self.t.start()


	def start_consuming(self):
		self.channel.start_consuming()

	def stop_consuming(self):
		self.channel.stop_consuming()

	def normalize(self, lst, l, r):
		range = max(lst) - min(lst)
		if range == 0:
			return lst
		else:
			result = []
			for item in lst:
				tmp = ((item - min(lst)) / range) * (r-l) + l
				result.append(tmp)
		return result


	def compute_probabilities(self, lst):
		A = np.array(lst)
		total = np.sum(A)
		each = np.sum(A,1)
		ratio = np.true_divide(each,total)
		return self.normalize(list(ratio), 0.1, 0.9)

	def compute_statistics(self):
		probabilities = self.compute_probabilities(self.zones)
		pprint(probabilities)

	def pass_tasks(self):
		for task in self.poll:
			data = dict(
						zone = task,
						probabilities = self.probabilities
					) 
			message = json.dumps(data)
			self.channel.basic_publish(exchange='',
	                      routing_key='task_queue',
	                      body=message,
	                      properties=pika.BasicProperties(
	                         delivery_mode = 2, # make message persistent
	                      ))

	def callback(self,ch, method, properties, body):
		out = json.loads(body)
		for i,zone in enumerate(out):
			self.zones[i].append(zone["hits"])
		self.n_tasks -= 1
		if self.n_tasks == 0:
			self.compute_statistics()
			self.stop_consuming()

		ch.basic_ack(delivery_tag = method.delivery_tag)


	def __del__(self):
		self.connection.close()

if __name__ == "__main__":
	rsc = RMQScheduler()
	rsc.pass_tasks()
